/** @type {import('@docusaurus/types').DocusaurusConfig} */

const math = require('remark-math');
const katex = require('rehype-katex');

module.exports = {
  title: 'Interpreting Ecological Soundscapes',
  tagline: '',
  url: 'https://research.kierangibb.co.uk',
  baseUrl: '/',
  favicon: 'img/logo.png',
  organizationName: 'm4gpie',
  projectName: 'interpreting-ecoacoustics',
  onBrokenLinks: 'throw',
  onBrokenMarkdownLinks: 'warn',
  themeConfig: {
    navbar: {
      title: 'Home',
      logo: {
        alt: '',
        src: 'img/logo.png',
      },
      items: [
        {
          to: 'blog/',
          label: 'Blog',
          position: 'left'
        },
        // {
        //   to: 'docs/',
        //   label: 'Documents',
        //   position: 'left'
        // },
      ],
    },
    footer: {
      style: 'dark',
      links: [
        {
          title: 'About',
          items: [
            {
              label: 'Email',
              href: 'mailto:kag25@sussex.ac.uk',
            },
            {
              label: 'Twitter',
              href: 'https://twitter.com/kieranagibb',
            },
            {
              label: 'Keybase',
              href: 'https://keybase.io/kyphae',
            },
          ],
        },
      ],
    },
  },
  presets: [
    [
      '@docusaurus/preset-classic',
      {
        docs: {
          sidebarPath: require.resolve('./sidebars.js'),
          editUrl: 'https://gitlab.com/m4gpie/phd-research/',
          remarkPlugins: [math],
          rehypePlugins: [katex],
        },
        blog: {
          showReadingTime: true,
          editUrl: 'https://gitlab.com/m4gpie/phd-research/',
          remarkPlugins: [math],
          rehypePlugins: [katex],
        },
        theme: {
          customCss: require.resolve('./src/css/custom.css'),
        },
      },
    ],
  ],
  stylesheets: [
    {
      href: 'https://cdn.jsdelivr.net/npm/katex@0.13.24/dist/katex.min.css',
      type: 'text/css',
      integrity: 'sha384-odtC+0UGzzFL/6PNoE8rX/SPcQDXBJ+uRepguP4QkPCm2LBxH3FA3y+fKSiJ+AmM',
      crossorigin: 'anonymous',
    },
  ],
};
